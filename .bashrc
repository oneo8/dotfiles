powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/local/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh

shopt -s checkwinsize

#Alias
alias fv='freebsd-version'
alias fu='freebsd-update'
alias tmux='tmux -2'
alias python='python3.7'
alias sphinx-build='sphinx-build-3.6'

# Paths
export PATH="$HOME/.config/composer/vendor/bin:$PATH"

# Environment Variables
export PYTHON='/usr/local/bin/python3.7'
