xfreerdp \
    /v:fcdc1zgrph2.co.franklin.oh.us \
    /d:fcdcdom1 \
    /u:rxtheven \
    /g:rdgateway.co.franklin.oh.us \
    /multimon \
    /monitors:0,1 \
    /gfx \
    +gfx-thin-client \
    +auto-reconnect \
    /compression-level:2 \
    /cert-tofu \
    /sound \
    +fonts \
    +aero \
    /microphone \
    /cert-ignore
